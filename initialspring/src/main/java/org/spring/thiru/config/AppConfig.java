package org.spring.thiru.config;

import java.util.Properties;
import org.springframework.core.env.Environment;
import static org.hibernate.cfg.Environment.*;

import org.spring.thiru.root.model.Authorities;
import org.spring.thiru.root.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
/**
 * 
 * 
 * @author ArasuDhanush
 *
 */

@Configuration
@PropertySources({ @PropertySource("classpath:db.properties"), @PropertySource("classpath:config.properties") })
@ComponentScans(value = { @ComponentScan(value = "org.spring.thiru.root.dao"), @ComponentScan(value = "org.spring.thiru.root.service") })
public class AppConfig {
	
	//org.springframework.core.env.Environment
	@Autowired
	private Environment env;
	
	@Bean
	public LocalSessionFactoryBean getSessionFactory() {
		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		Properties properties = new Properties();
		
		//JDBC
		properties.put(DRIVER, env.getProperty("mysql.driver"));
		properties.put(URL, env.getProperty("mysql.jdbcUrl"));
		properties.put(USER, env.getProperty("mysql.username"));
		properties.put(PASS, env.getProperty("mysql.password"));
		
		//HIBERNATE
		properties.put(SHOW_SQL, env.getProperty("hibernate.show_sql"));
		properties.put(HBM2DDL_AUTO, env.getProperty("hibernate.hbm2ddl.auto"));
		
		//C3P0
		properties.put(C3P0_MIN_SIZE, env.getProperty("hibernate.c3p0.min_size"));
		properties.put(C3P0_MAX_SIZE, env.getProperty("hibernate.c3p0.max_size"));
		properties.put(C3P0_ACQUIRE_INCREMENT, env.getProperty("hibernate.c3p0.acquire_increment"));
		properties.put(C3P0_TIMEOUT, env.getProperty("hibernate.c3p0.timeout"));
		properties.put(C3P0_MAX_STATEMENTS, env.getProperty("hibernate.c3p0.max_statements"));
		
		
		localSessionFactoryBean.setHibernateProperties(properties);
		localSessionFactoryBean.setAnnotatedClasses(Customer.class,Authorities.class);
		
		return localSessionFactoryBean;
	}
	
	@Bean
	public HibernateTransactionManager getTransactionManager() {
		HibernateTransactionManager hibernateTransactionManager = new HibernateTransactionManager();
		hibernateTransactionManager.setSessionFactory(getSessionFactory().getObject());
		return hibernateTransactionManager;
	}

}
