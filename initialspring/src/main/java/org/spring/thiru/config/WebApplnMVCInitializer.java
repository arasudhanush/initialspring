package org.spring.thiru.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
/**
 * 
 * @author ArasuDhanush
 *
 */
public class WebApplnMVCInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	// LOAD DATABASE AND SPRING SECURITY CONFIGURATION
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[]  { AppConfig.class, WebSecurityConfig.class };
	}

	// LOAD SPRING WEB CONFIGURATION
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { WebConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" } ;
	}

}
