package org.spring.thiru.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * 
 * @author ArasuDhanush
 *
 */
public class WebApplnSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
