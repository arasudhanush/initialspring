package org.spring.thiru.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 
 * @author ArasuDhanush
 *
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"org.spring.thiru.root.controller"})
public class WebConfig implements WebMvcConfigurer {
	
	public void configureViewResolver(ViewResolverRegistry  viewResolverRegistry) {
		viewResolverRegistry.jsp().prefix("/WEB-INF/views/").suffix("jsp");
	}
	
	public void addViewControllers(ViewControllerRegistry viewControllerRegistry) {
		viewControllerRegistry.addViewController("/login").setViewName("login");
	}
}
