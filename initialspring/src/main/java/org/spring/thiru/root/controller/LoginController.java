package org.spring.thiru.root.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 
 * @author ArasuDhanush
 *
 */

@Controller
public class LoginController {
	
	@GetMapping
	public String index(Model model, Principal principal) {
		model.addAttribute("message","You are logged in with " + principal.getName());
		return "index";
	}

}
