package org.spring.thiru.root.dao;

import org.springframework.security.core.userdetails.User;

/**
 * 
 * @author ArasuDhanush
 *
 */
public interface LoginDao {
	User findCustomerByCustomerName(String paramCustomerName);
}
