package org.spring.thiru.root.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author ArasuDhanush
 *
 */

@Repository
public class LoginDaoImpl implements LoginDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public User findCustomerByCustomerName(String paramCustomerName) {
		return sessionFactory.getCurrentSession().get(User.class, paramCustomerName);
	}

}
