package org.spring.thiru.root.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author ArasuDhanush
 *
 */


@Entity
@Table(name="AUTHORITIES")
public class Authorities {
	
	@Id
	@Column(name="AUTHORITY")
	private String authority;
	
	@ManyToOne
	@JoinColumn(name="CUSTOMERNAME")
	private Customer customer;

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	

}
