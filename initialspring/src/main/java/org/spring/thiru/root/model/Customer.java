package org.spring.thiru.root.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author ArasuDhanush
 *
 */

@Entity
@Table(name="CUSTOMERS")
public class Customer {
	
	@Id
	@Column(name="CUSTOMERNAME")
	private String customerName;
	
	@Column(name="PASSWORD")
	private String password;
	
	@Column(name="ENABLED")
	private String enabled;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "customer")
	private Set<Authorities> authorities = new HashSet<Authorities>();

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public Set<Authorities> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authorities> authorities) {
		this.authorities = authorities;
	}
}
