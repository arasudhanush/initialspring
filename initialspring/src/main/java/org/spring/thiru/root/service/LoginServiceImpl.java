package org.spring.thiru.root.service;


import org.spring.thiru.root.dao.LoginDaoImpl;
import org.spring.thiru.root.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoginServiceImpl implements UserDetailsService {
	
	@Autowired
	private LoginDaoImpl loginDaoImpl;

	@Transactional(readOnly = true )
	public UserDetails loadUserByUsername(String paramCustomerName) throws UsernameNotFoundException {
		User user = loginDaoImpl.findCustomerByCustomerName(paramCustomerName);
		UserBuilder userBuilder = null;
		if( null != user ) {
			userBuilder = User.withUsername(paramCustomerName);
			userBuilder.disabled(!user.isEnabled());
			userBuilder.password(user.getPassword());
			String[] authorities = user.getAuthorities()
					.stream().map(a -> a.getAuthority()).toArray(String[]::new);
			userBuilder.authorities(authorities);
		}else {
			throw new UsernameNotFoundException("Customer doesn't exists");
		}
		return userBuilder.build();
	}
}
